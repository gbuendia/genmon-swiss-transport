#!/usr/bin/env python3

# This script outputs a string ready to be used by the
# Xfce4 plugin "Generic Monitor" (genmon).
# It needs to be called with an argument, the stop ID, which will
# be used to query the Sear.ch API for a list of departures
# from said stop.

# THIS WORKS ONLY FOR SWISS STOPS AND A LIMITED NUMBER
# OF NON-SWISS STOPS FOR WHICH SEAR.CH PROVIDES DATA.

# To find out your stop ID:
# Go to https://timetable.search.ch/stop
# and start writing on the first field.
# Once you have found out how it is *exactly* written,
# go to https://timetable.search.ch/api/stationboard.json?stop=
# and add the stop name after the equals sign.
# The data resulting will contain an "id" key under "stop".

import urllib.request
import shutil
import json
from pathlib import Path
from datetime import datetime
from string import Template
from sys import argv
from typing import Tuple

########################################################################
# BASIC CONFIGURATION                                                  #
########################################################################

# In which folder will the timetable data be found/downloaded?
# The script will look for the icon here too.
# The path is relative to $HOME
DATA_FOLDER = ".local/share/genmon_swiss_transport/"

# Do you want to log when new data is downloaded?
LOGGING = True # Change to False if not

# In which folder will be the log kept?
# A folder different from the above mentioned data folder can be
# specified, if desired.
# The path is relative to $HOME
LOG_FOLDER = "DATA_FOLDER"

# Which command opens your preferred internet browser?
# Used to open the Sear.ch timetable page on click.
# Depending on your installation it can be "firefox", "google-chrome",
# "chromium-browser", "xdg-open", "sensible-browser"...
BROWSER = ""

########################################################################
# ADDITIONAL CONFIGURATION                                             #
########################################################################

# The script works alright if nothing below here is modified.

# If no arguments are passed, Sarnen will be used as default:
DEFAULT_STOPID = "8508314"

# Which name has/will be given the timetable data?
# Leave the extension out, as it will be added later,
# together with the stop id
TIMETABLE_FILE_NAME = "genmon_transportinfo"

# Which icon to use by default?
icon_type = "train"

# Which values are accepted for icon?
ICON_ACCEPTED = ["train", "bus"]

# Which name will be given to the log file?
# Leave the extension out, as it will be added later,
# together with the stop id
LOG_FILE_NAME = "genmon_transportinfo"

# After how much time should the timetable be re-downloaded?
# The json returned by Sear.ch contains about 1 day worth of data
# but the fresher a file the more likely to take into account
# last minute schedule changes.
# Dowloadig too frequently might get you banned from Sear.ch though.
# Keep this frequency at a sensible value:
DATA_IS_OLD_AFTER = 0.8 # hours

# Truncate long station names. Infinity to show the full name no matter.
# This is for the panel text only, not the tooltip. The hour and the
# ellipsis added afterwards are not counted.
MAX_CHARS_STOPNAME = float("inf") # Change this for an integer

TTURL = "https://timetable.search.ch/api/stationboard.json?stop="

GENMON_TEMPLATE = Template("<txt>$text</txt>"
                           + "<img>$icon</img>"
                           + "<tool>$tooltip</tool>"
                           + "$clicktag")

########################################################################
# DEFINITIONS                                                          #
########################################################################


def log_write(logmsg: str) -> None:
    if LOGGING:
        # We can't define the log file path before we check the arguments,
        # but we want to log if no arguments were found, that's why we
        # do it here.
        logfile = (str(Path.home())
                   + "/"
                   + LOG_FOLDER
                   + LOG_FILE_NAME
                   + "_"
                   + stopid
                   + ".log")
        logstring = f"{datetime.now()}\t" + logmsg + "\n"
        with open(logfile, 'a') as log_file:
            log_file.write(logstring)


def txtclickstring() -> str:
    # If we cannot find the browser, we'll leave the whole txtclick tag out
    txtclickstring = ""
    for command in [BROWSER, "sensible-browser", "xdg-open", "x-www-browser"]:
        if shutil.which(command) is not None:
            txtclickstring = "<txtclick>" \
                             + command \
                             + " " \
                             + human_url \
                             + "</txtclick>"
            break
    return txtclickstring


def download() -> None:
    # This is the best way I found at
    # https://stackoverflow.com/a/7244263
    # and https://stackoverflow.com/a/24226797
    req = urllib.request.Request(
            stopurl,
            data=None,
            headers={
                "User-Agent": "Xfce4 Panel Genmon plugin script: " \
                              + "https://gitlab.com/gbuendia/" \
                              + "genmon-swiss-transport"
                }
        )
    with urllib.request.urlopen(req) as response, \
         open(ttfile, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)
    log_write("Downloaded timetable data.")


def retrieve() -> dict:
    with open(ttfile) as json_file:
        json_data = json.load(json_file)
    return json_data


def todatetime(date_time: str) -> datetime:
    datetime_object = datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S")
    return datetime_object


def only_hhmm(date_time: str) -> str:
    datetime_object = todatetime(date_time)
    hhmm = f"{datetime_object.hour:02}:{datetime_object.minute:02}"
    return hhmm


def truncate(name: str) -> str:
    if len(name) > MAX_CHARS_STOPNAME:
        return name[:MAX_CHARS_STOPNAME] + "…"
    else:
        return name


def only_posterior() -> Tuple[str,list,list]:
    json_data = retrieve()
    stopname = "ERROR"
    if "stop" in json_data:
        stopname = json_data["stop"]["name"]
    if "connections" in json_data:
        departures = json_data["connections"]
        now = datetime.now()
        indexes_list = []
        for depindex in range(len(departures)):
            if todatetime(departures[depindex]["time"]) > now:
                indexes_list.append(depindex)
    else:
        # If there's no "connections" key, probably it's because
        # we got something else than a timetable, maybe a json with
        # just a list of error under the key "messages"
        # For the moment we just return five "departures" to populate
        # genmon with the "ERROR" string, but in the future we could use
        # the "messages" key to be more clear.
        indexes_list = [0,1,2,3,4]
        departures = []
        dummynow = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        for i in indexes_list:
            dummydata = {"time": dummynow, "terminal": {"name": "ERROR!"}}
            departures.append(dummydata)
        log_write("Malformed data")
    return stopname, departures, indexes_list


def file_too_old() -> bool:
    modified = Path(ttfile).lstat().st_mtime
    modified = datetime.fromtimestamp(modified)
    age = datetime.now() - modified
    age_hours = age.seconds / 3600
    if age_hours >= DATA_IS_OLD_AFTER:
        return True
    else:
        return False


def populate_genmon(stopname: str,
                    departures:list,
                    indexes_list:list) -> str:

    icon_path = str(Path.home()) \
                + "/" \
                + DATA_FOLDER \
                + "genmon_transport_" \
                + icon_type \
                + ".ico"

    genmon = GENMON_TEMPLATE

    next_departure = only_hhmm(departures[indexes_list[0]]["time"]) \
                     + " " \
                     + truncate(
                        departures[indexes_list[0]]["terminal"]["name"]
                        )

    five_departures = "<span weight='Bold'>" + stopname + "</span>\n\n"
    # We asume that the "connections" list is already served in
    # chronological order, so we just take the first five indexes
    # in our list of departures posterior to "now"
    for i in range(5):
        five_departures += only_hhmm(departures[indexes_list[i]]["time"]) \
                        + "\t" \
                        + departures[indexes_list[i]]["terminal"]["name"] \
                        + "\n"


    else:
        click_tag = ""

    genmon = genmon.substitute(text = next_departure,
                               icon = icon_path,
                               tooltip = five_departures,
                               clicktag = txtclickstring())
    return genmon


########################################################################
########################################################################

# Create the folder where the log will be written:
if LOGGING:
    Path(str(Path.home())
         + "/"
         + LOG_FOLDER).mkdir(parents=True,
                             exist_ok=True)

# The stop ID should be passed as argument.
if len(argv) < 2:
    # If there are no arguments, taking the default stop
    stopid = DEFAULT_STOPID
    log_write("No arguments passed, using default")
else:
    # If we get arguments, we take the first one as stop ID
    stopid = argv[1]
    # If there are more arguments, use the next one as icon.
    # If the icon name passed is not in the accepted list, don't
    # change the default.
    if len(argv) > 2 and argv[2] in ICON_ACCEPTED:
        icon_type = argv[2]
    # If there are more arguments, they are ignored

stopurl = TTURL + stopid

human_url = ("https://timetable.search.ch/"
             + stopid
             + "?time_type=depart")

# First we check if data already exist where we expect
ttfile = (str(Path.home())
          + "/"
          + DATA_FOLDER
          + TIMETABLE_FILE_NAME
          + "_"
          + stopid
          + ".json")
if Path(ttfile).is_file():
    # If we have data, get only what interests us:
    stopname, departures, indexes_list = only_posterior()
    # Use the interesting data to populate the string which
    # genmon will use, formatted according to its documentation at:
    # https://docs.xfce.org/panel-plugins/xfce4-genmon-plugin
    genmon = populate_genmon(stopname, departures, indexes_list)
    print(genmon)
    # Check if there are 5 or less departures posterior to now...
    if len(indexes_list) <= 5:
        # ...in which case we download a fresh data file...
        log_write("Too few indexes.")
        download()
    # ...likewise if a sensible amount of time has passed
    # since the last download:
    elif file_too_old():
        log_write("File too old.")
        download()
else:
    log_write("No data file found.")
    # Create the path before downloading
    Path(str(Path.home())
         + "/"
         + DATA_FOLDER).mkdir(parents=True,
                              exist_ok=True)
    download()
