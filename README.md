## Description

This script outputs a string ready to be used by the Xfce4 panel plugin
"Generic Monitor" (genmon).

It will download the departure data for a given station from the 
[Sear.ch](https://www.search.ch/) [timetable](https://timetable.search.ch/stop) 
[API](https://timetable.search.ch/api/help)
and display the next departure on the panel and the next five ones in a tooltip.

![Screenshot](screenshot.png "Screenshot")*Two instances of genmon running
the script, one of them showing the tooltip.*

## Installation

### Prerequisites

- [Xfce panel](https://docs.xfce.org/xfce/xfce4-panel/start)
- [Genmon](https://docs.xfce.org/panel-plugins/xfce4-genmon-plugin)
- [Python 3](https://www.python.org/)

### Instructions

1. Copy `transportinfo.py` to a convenient place and make sure it's executable.
1. Copy all \*.ico files to a convenient place *which you will have to use in the configuration of the script*. Or use `~/.local/share/genmon_swiss_transport` if you want to skip configuration.

### Configuration

1. Open `transportinfo.py` in your favourite text editor.
1. Define `DATA_FOLDER` if you are using a different one from the default.
1. Define the constants you deem necessary.

### Finding your Stop ID.

1. Go to [Sear.ch's timetable](https://timetable.search.ch/stop) and start writing in the first field. Let the autocomplete do its magic. Now you will know how the stop name is *exactly* written.
1. Go to `https://timetable.search.ch/api/stationboard.json?stop=` appending the stop name after the equals sign.
1. You will get a json document. Look for the key `id` under `stop`. That's the number you are looking for.

## Usage

1. Go to the Xfce panel preferences and add a genmon item to the panel of your choice.
1. Go to the item configuration (cogwheel icon) and fill the "Command" field with the script path followed by the following arguments:
   1. Stop ID. See above in this Readme to find out this value.
   1. Icon. For the moment it can just be `train` or `bus`. More to be added soon.
1. The period of 30.00 seconds should suffice as the granularity of the timetables is of 1 minute.

Example command: `/home/yourusername/.local/bin/genmon_scripts/transportinfo.py 8508314 train`

If all goes well, after a little while you should see something similar to the screenshot above.

Mouseover the panel item to get the tooltip.

Click on the panel item to open Sear.ch's timetable page for that stop.
